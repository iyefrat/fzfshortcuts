"""
command line arguments and configuration file parser
"""
import os
import sys
import configparser
from argparse import ArgumentParser
import logging
logger = logging.getLogger(__name__)

from . import __version__

class Argpconf():
    def __init__(self):
        """
        Define command line arguments parser
        """
        self.argp = ArgumentParser(
            description="Generator for shell, ranger and vim commands for fzf based shortcuts"
        )
        self.argp.add_argument(
            '-d', '--definitions',
            help='definitions configuration file',
            metavar='DEFINITIONS',
            nargs=1,
            type=str
        )
        self.argp.add_argument(
            '-g', '--generator',
            metavar='GENERATOR',
            nargs=1,
            help='use a specific template from the templates directory'
        )
        self.argp.add_argument(
            '-t', '--templates',
            metavar='TEMPLATES',
            nargs=1,
            help='directory to search for templates'
        )
        self.argp.add_argument(
            '-c', '--config',
            metavar='CONFIG',
            nargs=1,
            help='directory to search for templates'
        )
        self.argp.add_argument(
            '-v', '--verbose',
            dest='verbosity',
            default=2,
            action='count',
            help='increase verbosity, use multiple times to increase more'
        )
        self.argp.add_argument(
            '-V', '--version',
            action='version',
            version='%(prog)s {}'.format(__version__),
            help='Display version and exit'
        )
        self.args = vars(self.argp.parse_args())
    def setup_defaults(self):
        """
        Parse command line arguments and configuration file
        """
        logger.setLevel(self.get_loglevel())
        logger.addHandler(self.get_loghandler())
        if self.args['config'] is None:
            logger.info("searching for configuration file in the default location, according to"
                         "environment")
            default_config_dir_suffix = '/fzfshortcuts'
            try:
                default_config_dir_prefix = os.environ['XDG_CONFIG_HOME']
            except KeyError:
                default_config_dir_prefix = os.environ['HOME'] + '/.config'
            default_config_dir = default_config_dir_prefix + default_config_dir_suffix
            logger.debug("using default location for all configuration values: %s", default_config_dir)
            default_config_file = default_config_dir + "/config.ini"
            config_file = default_config_file
            logger.debug("using default location main configuration file: %s", default_config_file)
        else:
            config_file = self.args['config'][0]
        self.config = configparser.ConfigParser()
        try:
            logger.info("reading configuration file %s", config_file)
            self.config.read(config_file)
            if not ('main' in self.config and 'save' in self.config):
                if os.path.isfile(config_file):
                    logger.fatal("no configuration file was found here: %s, but no [save] and [main] "
                                 "sections were found, please read the documentation", config_file)
                    sys.exit(2)
                else:
                    logger.fatal("configuration file %s was not found, please read the documentation",
                                 config_file)
                    sys.exit(1)
        except configparser.ParsingError:
            logger.fatal("configuration file %s has syntax errors, please consult documentation",
                         config_file)
            sys.exit(2)
        if self.args['templates'] is None:
            self.args['templates'] = os.path.expanduser(os.path.expandvars(
                self.config.get('main', 'templates',
                           fallback=default_config_dir + "/templates")
            ))
        if self.args['definitions'] is None:
            self.args['definitions'] = os.path.expanduser(os.path.expandvars(
                self.config.get('main', 'definitions',
                           fallback=default_config_dir + "/definitions.json")
            ))
        self.args['config'] = config_file
        try:
            self.args['save'] = self.config['save']
        except KeyError:
            logger.fatal("No [save] section was configured in %s", self.args['config'])
            sys.exit(2)
    def get_loglevel(self):
        # Convert logging level to the proper scale
        logging_level = self.args['verbosity'] * 10
        # make sure a level higher then 50 won't be passed to setLevel
        logging_level = min(logging_level, 50)
        # reverse the numbers of so a higher verbosity will set a higher log level
        logging_level = 60 - logging_level
        return logging_level
    def get_logformat(self):
        if self.args['verbosity'] > 2:
            return logging.Formatter('%(name)s - %(levelname)s - %(message)s')
        else:
            return logging.Formatter('%(message)s')
    def get_loghandler(self):
        handler = logging.StreamHandler()
        handler.setFormatter(self.get_logformat())
        return handler
