""" Main application entry point.

    python -m fzfshortcuts  ...

"""

from .cli import main as cli

def main():
    """ Execute the application.

    """
    cli()


# Make the script executable.

if __name__ == "__main__":
    raise SystemExit(main())
