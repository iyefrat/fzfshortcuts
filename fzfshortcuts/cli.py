""" Implementation of the command line interface.

"""

import os

from .api import render_template
from .core import argpconf, Logger
logger = Logger(__name__)

def main():
    argpconf.setup_defaults()
    args = argpconf.args
    if args['generator'] is None:
        generators_to_render = args['save']
    else:
        generators_to_render = [args['generator']]
    for generator in generators_to_render:
        outfile = open(os.path.expanduser(os.path.expandvars(args['save'][generator])), 'w')
        logger.debug("opening output file %s for configured generator %s",
                     outfile.name, generator)
        outfile.write(render_template(args['templates'] + "/" + generator, args['definitions']))
        logger.debug("finished writing rendered template of generator %s to %s",
                     generator, outfile.name)
        outfile.close()
    return 0

# Make the module executable.

if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        logger.critical("keyboard interrupt")
